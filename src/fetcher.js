import axios from 'axios';


/* Get data from SWAPI simply :
 * params :
 * - urlOrType : a full SWAPI url or one of its 6 main categories
 * - i : if you gave a category above, give an index to fetch precisely
 *  (example : fetcher('films', 2); is equivalent to fetch(https://swapi.dev/api/films/2);)
 */
export default async function fetcher(urlOrType, i = '') {
    const suffix = (i === '') ? '' : '/' + i;
    const url = (urlOrType.startsWith('https')) ? urlOrType : `https://swapi.dev/api/${urlOrType}${suffix}`;
    try {
        const res = await axios.get(url);
        return res.data;
    }
    catch(err) {
        return 'error : ' + err;
    }
}
