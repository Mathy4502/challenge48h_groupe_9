# 📑 Challenge 48H

This project is a swapi-based platform to organize and present all information from the star-wars universe.

The user will be able to see each character, each vehicle, each planet... There is also a play area with a little game about Star-Wars

## ☝️ Prerequisites

Before we start, please make sure you have an internet connection !

# 🟢 Launching

Open your browser and go to:

🔸 `http://141.95.151.77/src/`

# 📥 Download

If you want to download this project :

- Release version 👉 [here](https://gitlab.com/Mathy4502/challenge48h_groupe_9.git)

# 👥 Team

Project carried out at Nantes Ynov Campus by the learners of TEAM5 (The Padawans) Computer Science 2022

- MENARD Mathias [@Mathy4502](https://gitlab.com/Mathy4502)

- FORAFO Damien aka [@damien.forafo](https://gitlab.com/damien.forafo)

- ACHARD Lisa aka [@Lisachard](https://gitlab.com/Lisachard)

- LEROY Mathias aka [@MathLry](https://gitlab.com/MathLry)

- TOURABI Mostapha aka [@Mostapha44](https://gitlab.com/Mostapha44)

- SCHNEIDER Nathan`` aka [@NatSch45](https://gitlab.com/NatSch45)

---

_Nantes YNOV Campus - CHALLENGE 48H - Info - 2022_
